import { AuthGuard } from './shared/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';

const routes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  {
    path: '',
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'banned',
        loadChildren: './banned/banned.module#BannedModule',
        canLoad: [AuthGuard],
      },
      {
        path: 'profile',
        loadChildren: './profile/profile.module#ProfileModule',
        canLoad: [AuthGuard],
      },
      {
        path: 'users',
        loadChildren: './users/users.module#UsersModule',
        // canLoad: [AuthGuard],
      },
    ],
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [RouterModule.forRoot(
    routes,
    { preloadingStrategy: PreloadAllModules }
  )],
  declarations: []
})
export class RoutingModule { }
