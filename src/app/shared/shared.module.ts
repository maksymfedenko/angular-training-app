import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { LoaderComponent } from './components/loader/loader.component';
import { TogglerComponent } from './components/toggler/toggler.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { NavSidebarComponent } from './components/nav-sidebar/nav-sidebar.component';
import { HeaderComponent } from './components/header/header.component';
import { OnlyAdminDirective } from './directives/only-admin.directive';
import { AutoFocusCustomDirective } from './directives/auto-focus-custom.directive';
import { RouterModule } from '@angular/router';
import { PalingPipe } from './pipes/paling.pipe';
import { CounterComponent } from './components/counter/counter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  declarations: [
    OnlyAdminDirective,
    AutoFocusCustomDirective,
    LoaderComponent,
    TogglerComponent,
    SidebarComponent,
    NavSidebarComponent,
    HeaderComponent,
    NotFoundComponent,
    PalingPipe,
    CounterComponent,
  ],
  providers: [],
  exports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoaderComponent,
    TogglerComponent,
    SidebarComponent,
    NavSidebarComponent,
    NotFoundComponent,
    HeaderComponent,
    OnlyAdminDirective,
    AutoFocusCustomDirective,
    PalingPipe,
    CounterComponent,
  ]
})
export class SharedModule { }
