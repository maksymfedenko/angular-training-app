import { AuthService } from '../../core/auth/auth.service';
import { Injectable } from '@angular/core';
import { CanDeactivate, CanActivate, CanLoad, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanDeactivate<any>, CanActivate, CanLoad, CanActivateChild {
  constructor(private authService: AuthService) { }

  canDeactivate(): Observable<boolean> {
    return this.authService.validateToken();
  }

  canActivate(): Observable<boolean> {
    return this.authService.validateToken().pipe(
      tap((isAuthorized) => this.redirectIfNotAuthorized(isAuthorized))
    );
  }

  canActivateChild(): Observable<boolean> {
    return this.authService.validateToken().pipe(
      tap((isAuthorized) => this.redirectIfNotAuthorized(isAuthorized))
    );
  }

  canLoad(): Observable<boolean> {
    return this.authService.validateToken();
  }

  private redirectIfNotAuthorized(isAuthorized: boolean): void {
    if (!isAuthorized) {
      this.authService.logout();
    }
  }
}
