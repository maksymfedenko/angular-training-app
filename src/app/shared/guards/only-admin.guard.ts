import { tap, map, take } from 'rxjs/operators';
import { AuthService } from '../../core/auth/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class OnlyAdminGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }

  canActivate(): Observable<boolean> {
    return this.authService.currentProfile$.pipe(
      take(1),
      map((profile) => profile.isAdmin),
      tap((isAdmin) => {
        if (!isAdmin) {
          this.router.navigate(['/notFound']);
        }
      })
    );
  }
}
