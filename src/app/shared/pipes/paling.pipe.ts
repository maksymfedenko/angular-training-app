import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paling'
})
export class PalingPipe implements PipeTransform {

  transform(value: string = ''): string {
    return value.split('').map((letter: string, index: number): string => {
      return index % 2 ? letter.toLowerCase() : letter.toUpperCase();
    }).join('');
  }

}
