import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../../../core/auth/auth.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Profile } from '../../../shared/models/profile.model';
import { Unsubscriber } from '../../classes/unsubscriber.class';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends Unsubscriber implements OnInit {
  @Output() logoClick = new EventEmitter<void>();
  profile: Profile;

  constructor(private authService: AuthService) {
    super();
  }

  ngOnInit() {
    this.authService.currentProfile$.pipe(
      takeUntil(this.componetDestroyed)
    ).subscribe(
      (profile) => this.profile = profile
    );
  }

  onLogoClick() {
    this.logoClick.emit();
  }
}
