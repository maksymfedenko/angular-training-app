import { Component, OnInit, Input, OnChanges, SimpleChanges, Renderer2, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription, fromEvent } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnChanges, OnDestroy {
  @Input() open: boolean;
  @Output() clickOutside = new EventEmitter<void>();
  private clickListener: () => void;
  private rxClickListener: Subscription;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (!changes.open || changes.open.currentValue === changes.open.previousValue) {
      return;
    }

    if (changes.open.currentValue) {
      setTimeout(() => {

        // angular way
        // this.clickListener = this.renderer.listen('body', 'click', (event) => this.handleClick(event));

        // RxJS way
        const clicks = fromEvent(document, 'click');
        this.rxClickListener = clicks.subscribe((event: any) => this.handleClick(event));
      }, 0);
    } else {
      this.removeListener();
    }
  }

  ngOnDestroy() {
    this.removeListener();
  }

  private removeListener() {
    if (this.clickListener) {
      this.clickListener();
    }

    if (this.rxClickListener) {
      this.rxClickListener.unsubscribe();
    }
  }

  private handleClick(event: any) {
    const isClickedInsideSidebar = event.composedPath().some((el) =>
      el.classList && [].includes.call(el.classList, 'sidebar')
    );

    if (!isClickedInsideSidebar) {
      this.clickOutside.emit();
    }
  }
}
