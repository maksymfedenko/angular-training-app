import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-nav-sidebar',
  templateUrl: './nav-sidebar.component.html',
  styleUrls: ['./nav-sidebar.component.scss']
})
export class NavSidebarComponent implements OnInit {
  @Input() open: boolean;
  @Output() clickOutside = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  onClickOutside() {
    this.clickOutside.emit();
  }
}
