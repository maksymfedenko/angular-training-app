import { Observable } from 'rxjs';
import { getClicksCount, getLoading } from '../../../store/clicks/clicks.selector';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../store';
import {
  Increment,
  Decrement,
  IncrementAsync,
} from '../../../store/clicks/clicks.action';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  clicksCount$: Observable<number>;
  loading$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
    this.clicksCount$ = this.store.select(getClicksCount);
    this.loading$ = this.store.select(getLoading);
  }

  ngOnInit() {
  }

  plus() {
    this.store.dispatch(new Increment());
  }

  minus() {
    this.store.dispatch(new Decrement());
  }

  plusAsync() {
    this.store.dispatch(new IncrementAsync());
  }
}
