export class Profile {
  id?: string;
  username: string;
  password: string;
  isAdmin: boolean;
  avatar?: string;
  lastToken?: string;

  constructor(data: Profile) {
    this.username = data.username;
    this.password = data.password;
    this.isAdmin = data.isAdmin;
    this.avatar = data.avatar;
  }
}

export interface ProfileLogin {
  username: string;
  password: string;
}
