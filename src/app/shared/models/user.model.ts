export class User {
  id: number;
  name: string;
  username: string;
  banned?: boolean;

  constructor(data: User) {
    this.id = data.id;
    this.name = data.name;
    this.username = data.username;
    this.banned = data.banned;
  }
}
