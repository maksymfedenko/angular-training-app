import { Directive, ElementRef, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appAutoFocusCustom]'
})
export class AutoFocusCustomDirective implements OnInit {

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit(): void {
    this.elementRef.nativeElement.focus();
    // this.renderer.setStyle(this.elementRef.nativeElement, 'background', 'blue');
  }

}
