import { Unsubscriber } from './../classes/unsubscriber.class';
import { Directive, OnInit, HostBinding, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[appOnlyAdmin]'
})
export class OnlyAdminDirective extends Unsubscriber implements OnInit, OnDestroy {
  @HostBinding('style.display') display = 'block';
  private currentProfileChanges: Subscription;

  constructor(private authService: AuthService) {
    super();
  }

  ngOnInit(): void {
    this.currentProfileChanges = this.authService.currentProfile$.pipe(
      takeUntil(this.componetDestroyed)
    ).subscribe((profile) => {
      this.setElementVisibility(profile.isAdmin);
    });
  }

  setElementVisibility(isAdmin: boolean): void {
    this.display = isAdmin ? 'block' : 'none';
  }
}
