import { finalize } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../core/users/users.service';
import { User } from '../../../shared/models/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  isLoading = false;
  users: User[] = [];
  filteredUsers: User[] = [];

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  private getUsers(): void {
    this.isLoading = true;

    this.usersService.getUsers().pipe(
      finalize(() => {
        this.isLoading = false;
      })
    )
      .subscribe(users => {
        this.users = users;
        this.setFilteredUsers();
      });
  }

  setFilteredUsers(search: string = ''): void {
    if (!search) {
      this.filteredUsers = this.users;
    }

    const regex = new RegExp(search, 'i');
    this.filteredUsers = this.users.filter((user) => user.name.match(regex));
  }

}
