// import { User } from '../../../shared/models/user.model';
// import { of } from 'rxjs';
// import { FormsModule } from '@angular/forms';
// import { LoaderComponent } from '../../../shared/components/loader/loader.component';
// import { UsersService } from '../../../core/services/users/users.service';
// import { RouterTestingModule } from '@angular/router/testing';
// import { async, ComponentFixture, TestBed, ComponentFixtureAutoDetect } from '@angular/core/testing';

// import { AddUserComponent } from './add-user.component';

// const testUser: User = {
//   id: 1,
//   username: 'testUser',
//   name: 'testname',
//   banned: false,
// };

// describe('AddUserComponent', () => {
//   let component: AddUserComponent;
//   let fixture: ComponentFixture<AddUserComponent>;
//   let usersServiceSpy: jasmine.SpyObj<UsersService>;
//   let addUserSpy;

//   beforeEach(async(() => {
//     const spy = jasmine.createSpyObj('UsersService', ['addUser']);
//     addUserSpy = spy.addUser.and.returnValue( of(testUser) );

//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         FormsModule,
//       ],
//       declarations: [ AddUserComponent, LoaderComponent ],
//       providers: [
//         {
//           provide: UsersService,
//           useValue: spy,
//         },
//         { provide: ComponentFixtureAutoDetect, useValue: true }
//       ],
//     })
//     .compileComponents();

//     usersServiceSpy = TestBed.get(UsersService);

//     fixture = TestBed.createComponent(AddUserComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   }));

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });

//   describe('#submit btn', () => {
//     let submitBtn: HTMLElement;
//     let hostElement: HTMLElement;
//     let nameInput: HTMLInputElement;
//     let usernameInput: HTMLInputElement;

//     beforeEach(() => {
//       hostElement = fixture.nativeElement;
//       submitBtn = hostElement.querySelector('button[type="submit"]');
//       nameInput = hostElement.querySelector('input[name="name"]');
//       usernameInput = hostElement.querySelector('input[name="username"]');
//     });

//     it('should create', () => {
//       expect(submitBtn).toBeDefined();
//     });

//     it('should be disabled', () => {
//       const disabled = submitBtn.getAttribute('disabled');
//       expect(disabled).toEqual('');
//     });

//     it('should be active when form filled', () => {
//       nameInput.value = testUser.name;
//       usernameInput.value = testUser.username;
//       nameInput.dispatchEvent(new Event('input'));
//       usernameInput.dispatchEvent(new Event('input'));

//       fixture.detectChanges();
//       const disabled = submitBtn.getAttribute('disabled');
//       expect(disabled).toBeNull();
//     });
//   });

//   describe('#form submit', () => {
//     let submitBtn: HTMLElement;
//     let hostElement: HTMLElement;
//     let nameInput: HTMLInputElement;
//     let usernameInput: HTMLInputElement;

//     beforeEach(() => {
//       hostElement = fixture.nativeElement;
//       submitBtn = hostElement.querySelector('button[type="submit"]');
//       nameInput = hostElement.querySelector('input[name="name"]');
//       usernameInput = hostElement.querySelector('input[name="username"]');

//       nameInput.value = testUser.name;
//       usernameInput.value = testUser.username;
//       nameInput.dispatchEvent(new Event('input'));
//       usernameInput.dispatchEvent(new Event('input'));
//     });

//     it('should change submitting to true', () => {
//       fixture.detectChanges();
//       submitBtn.dispatchEvent(new Event('click'));
//       expect(disabled).toBeNull();
//     });
//   });
// });
