import { User } from '../../../shared/models/user.model';
import { UsersService } from '../../../core/users/users.service';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  newUser: User = {
    id: Math.round(Math.random() * 990) + 10,
    name: '',
    username: '',
    banned: false,
  };
  submitting = false;
  submitError = '';

  constructor(private usersService: UsersService, private location: Location) { }

  ngOnInit() {
  }

  goBack(): void {
    this.location.back();
  }

  submit(): Subscription {
    this.submitting = true;
    this.submitError = '';

    return this.usersService.addUser(this.newUser)
      .subscribe(
        () => this.goBack(),
        (err) => {
          this.submitting = false;
          this.submitError = err.message;
        }
      );
  }

  onFileLoad(target: any) {
    const file = target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsText(file, 'UTF-8');
      reader.onload = (evt: any) => {
        const userData = evt.target.result.split('\n').filter((line) => line).map((line) => line.split(';').filter((cell) => cell))[0];
        console.info('userData from file', userData); // tslint:disable-line no-console
        if (userData.length > 1) {
          this.newUser.name = userData[0];
          this.newUser.username = userData[1];
        }
      };
      reader.onerror = function (evt) {
        console.error('error', evt);
      };
    }
  }
}
