import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { AddUserComponent } from './components/add-user/add-user.component';
import { OnlyAdminGuard } from '../shared/guards/only-admin.guard';

const routes: Routes = [
  { path: '', component: UserListComponent },
  { path: 'new', component: AddUserComponent },
  { path: ':id', component: UserDetailComponent, canActivate: [OnlyAdminGuard] },
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [RouterModule.forChild(routes)],
  declarations: []
})
export class UsersRoutingModule { }
