import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store';
import * as fromClicks from './clicks/clicks.reducer';

export interface State {
  clicks: fromClicks.State;
}

export const reducers: ActionReducerMap<State> = {
  clicks: fromClicks.reducer,
};

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function (state: State, action: any): State {
    console.log('action', action);
    return reducer(state, action);
  };
}
export const metaReducers: MetaReducer<State>[] = [logger];
