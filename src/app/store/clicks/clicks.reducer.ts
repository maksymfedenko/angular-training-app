import * as ClicksAction from './clicks.action';

export interface State {
  clicksCount: number;
  loading: boolean;
}

export const initialState: State = {
  clicksCount: 0,
  loading: false,
};

export function reducer(state = initialState, action: ClicksAction.Action): State {
  switch (action.type) {
    case ClicksAction.INCREMENT: {
      return {
        ...state,
        clicksCount: state.clicksCount + action.payload,
      };
    }

    case ClicksAction.DECREMENT: {
      return {
        ...state,
        clicksCount: state.clicksCount - action.payload,
      };
    }

    case ClicksAction.INCREMENT_ASYNC: {
      return {
        ...state,
        loading: true,
      };
    }

    case ClicksAction.INCREMENT_ASYNC_SUCCESS: {
      return {
        ...state,
        clicksCount: state.clicksCount + action.payload,
        loading: false,
      };
    }

    default:
      return state;
  }
}
