import { Injectable } from '@angular/core';

import { Effect, Actions } from '@ngrx/effects';
import { delay, map } from 'rxjs/operators';

import * as clicksActions from './clicks.action';

@Injectable()
export class ClicksEffects {
  constructor(private actions$: Actions) {}

  @Effect()
  loadMovies$ = this.actions$.ofType(clicksActions.INCREMENT_ASYNC).pipe(
    delay(1500),
    map((action: clicksActions.IncrementAsync) => {
      return new clicksActions.IncrementAsyncSuccess(action.payload);
    }),
  );
}
