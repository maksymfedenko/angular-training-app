import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State } from './clicks.reducer';


export const getClicksState = createFeatureSelector<State>('clicks');

export const getClicksCount = createSelector(
  getClicksState,
  (state: State) => state.clicksCount,
);

export const getLoading = createSelector(
  getClicksState,
  (state: State) => state.loading,
);
