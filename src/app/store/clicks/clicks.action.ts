import { Action } from '@ngrx/store';

export const INCREMENT = '[Clicks] INCREMENT';
export const DECREMENT = '[Clicks] DECREMENT';
export const INCREMENT_ASYNC = '[Clicks] INCREMENT_ASYNC';
export const INCREMENT_ASYNC_SUCCESS = '[Clicks] INCREMENT_ASYNC_SUCCESS';


export class Increment implements Action {
  readonly type: string = INCREMENT;
  constructor(public payload: number = 1) { }
}

export class IncrementAsync extends Increment {
  readonly type = INCREMENT_ASYNC;
}

export class IncrementAsyncSuccess extends Increment {
  readonly type = INCREMENT_ASYNC_SUCCESS;
}

export class Decrement implements Action {
  readonly type = DECREMENT;
  constructor(public payload: number = 1) { }
}

export type Action = Increment | Decrement;
