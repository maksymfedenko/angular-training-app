import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private toastrService: ToastrService) { }

  toastSuccess(message: string, title?: string): void {
    this.toastrService.success(message, title);
  }

  toastInfo(message: string, title?: string): void {
    this.toastrService.info(message, title);
  }
}
