import { Profile, ProfileLogin } from '../../shared/models/profile.model';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from './auth.service';
import { MessageService } from '../message/message.service';
import { omit } from 'lodash';

const defaultProfiles: Profile[] = [
  {
    id: '1',
    username: 'testuser',
    password: '1',
    isAdmin: true,
  },
  {
    id: '2',
    username: 'testuser2',
    password: '1',
    isAdmin: false,
    lastToken: 'fake_token',
  }
];

fdescribe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [
        {
          provide: MessageService,
          useValue: {
            toastSuccess(): void { },
            toastInfo(): void { },
          }
        },
      ],
    });

    localStorage.setItem('token', '');
    localStorage.setItem('profiles', JSON.stringify(defaultProfiles));
    service = TestBed.get(AuthService);
    spyOn(service.router, 'navigate').and.returnValue(true);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('#login', () => {
    let testUser: Profile;
    let loginData: ProfileLogin;

    beforeAll(() => {
      testUser = { ...defaultProfiles[0] };
      loginData = {
        username: testUser.username,
        password: testUser.password,
      };
    });

    it('should failed', (done: DoneFn) => {
      service.login({
        ...loginData,
        password: 'invalid_password',
      }).subscribe(() => { }, (err: Error) => {
        expect(err.message).toEqual('Login failed.');
        done();
      });
    });

    it('should success', (done: DoneFn) => {
      service.login(loginData).subscribe(profile => {
        expect(profile.id).toBe(testUser.id);
        done();
      }, (err) => console.log('!!!22', typeof err));
    });

    it('should set token to profile', (done: DoneFn) => {
      service.login(loginData).subscribe(profile => {
        expect(profile.lastToken).not.toBe(testUser.lastToken);
        done();
      });
    });

    it('should set token to storage equal to profile last token', (done: DoneFn) => {
      service.login(loginData).subscribe(profile => {
        expect(profile.lastToken).toBe(localStorage.getItem('token'));
        done();
      });
    });
  });

  describe('#logout', () => {
    let testUser: Profile;
    let loginData: ProfileLogin;

    beforeAll(() => {
      testUser = { ...defaultProfiles[0] };
      loginData = {
        username: testUser.username,
        password: testUser.password,
      };
    });

    beforeEach((done: DoneFn) => {
      service.login(loginData).subscribe(() => {
        done();
      });
    });

    it('should remove token', () => {
      expect(localStorage.getItem('token')).toBeTruthy();
      service.logout();
      expect(localStorage.getItem('token')).toBeFalsy();
    });
  });

  describe('#validateToken', () => {
    const testToken = 'test_token';
    const testUserIndex = 0;

    beforeEach(() => {
      JSON.parse(localStorage.getItem('profiles'))[testUserIndex].lastToken = testToken;
    });

    it('valid token', (done: DoneFn) => {
      localStorage.setItem('token', testToken);
      service.validateToken().subscribe(isAuthorized => {
        expect(isAuthorized).toBeTruthy();
        done();
      });
    });

    it('invalid token', (done: DoneFn) => {
      localStorage.setItem('token', 'invalid token');
      service.validateToken().subscribe(isAuthorized => {
        expect(isAuthorized).toBeFalsy();
        done();
      });
    });
  });

  describe('#updateProfile', () => {
    let testUser: Profile;
    let updateData: Profile;
    let loginData: ProfileLogin;

    beforeAll(() => {
      testUser = { ...defaultProfiles[0] };
      loginData = {
        username: testUser.username,
        password: testUser.password,
      };
      updateData = {
        ...testUser,
        avatar: 'updated_avatar',
        username: 'updated_username',
      };
    });

    beforeEach((done: DoneFn) => {
      service.login(loginData).subscribe(() => {
        done();
      });
    });

    it('should return correct user', (done: DoneFn) => {
      service.updateProfile(updateData).subscribe(profile => {
        expect(profile.id).toBe(testUser.id);
        done();
      });
    });

    it('should update user data', (done: DoneFn) => {
      service.updateProfile(updateData).subscribe(profile => {
        expect(omit(profile, ['lastToken'])).toEqual(updateData);
        done();
      });
    });
  });

});
