import { LocalStorage } from 'ngx-webstorage';
import { MessageService } from '../message/message.service';
import { ProfileLogin, Profile } from '../../shared/models/profile.model';
import { Injectable } from '@angular/core';
import uuid from 'uuid/v4';
import { Router } from '@angular/router';
import { throwError, Observable, of, BehaviorSubject } from 'rxjs';
import { delay, tap } from 'rxjs/operators';

const defaultProfile: Profile = {
  username: '__not_logined',
  password: '1',
  isAdmin: false,
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentProfile$: BehaviorSubject<Profile> = new BehaviorSubject(defaultProfile);

  @LocalStorage() private token: string;
  @LocalStorage() private profiles: Profile[];

  constructor(public router: Router, private messageService: MessageService) { }

  login(data: ProfileLogin): Observable<Profile> {
    const profiles = this.getProfiles();

    const currentProfile = profiles.find(
      (profile) => data.password === profile.password && data.username === profile.username
    );

    if (!currentProfile) {
      return throwError(new Error('Login failed.'));
    }

    const token: string = uuid();

    currentProfile.lastToken = token;

    this.profiles = profiles;
    this.token = token;

    this.currentProfile$.next(currentProfile);

    return of(currentProfile).pipe(
      delay(1000)
    );
  }

  logout() {
    this.currentProfile$.next(defaultProfile);
    this.token = '';
    this.router.navigate(['/login']);
  }

  validateToken(): Observable<boolean> {
    const profiles = this.getProfiles();

    const currentProfile = profiles.find(
      (profile) => this.token === profile.lastToken
    );

    this.currentProfile$.next(currentProfile || defaultProfile);
    return of(Boolean(currentProfile));
  }

  updateProfile(profileData: Profile): Observable<Profile> {
    const profiles = this.getProfiles();

    const currentProfileIndex = profiles.findIndex(
      (profile) => this.token === profile.lastToken
    );

    const currentProfile = {
      ...this.currentProfile$.getValue(),
      ...profileData,
    };

    profiles[currentProfileIndex] = currentProfile;

    this.currentProfile$.next(currentProfile);
    this.profiles = profiles;
    return of(currentProfile).pipe(
      delay(1000),
      tap(() => this.messageService.toastSuccess('Profile Updated!')),
    );
  }

  private getProfiles(): Profile[] {
    const profiles: Profile[] = this.profiles;

    if (!profiles) {
      return this.setDefaultProfiles();
    }

    return profiles;
  }

  private setDefaultProfiles(): Profile[] {
    const defaultProfiles: Profile[] = [
      {
        id: '1',
        username: 'admin',
        password: '123456',
        isAdmin: true,
        avatar: 'http://www.ivuzee.co/assets/img/avatars/profiles/avatar-6.png',
      },
      {
        id: '2',
        username: 'user',
        password: '123456',
        isAdmin: false,
        avatar: 'https://cannakitchen.org/wp-content/uploads/2014/12/admin_avatar_1419208227-96x96.jpg',
      }
    ];

    this.profiles = defaultProfiles;
    return defaultProfiles;
  }
}
