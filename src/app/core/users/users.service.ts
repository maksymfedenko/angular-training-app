import { MessageService } from '../message/message.service';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { User } from '../../shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { catchError, tap, delay, map } from 'rxjs/operators';
import { uniqBy } from 'lodash';
import { LocalStorage } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private users: User[] = [];
  @LocalStorage() private usersFromStorage: User[] = [];

  constructor(private http: HttpClient, private messageService: MessageService) { }

  private heroesUrl = 'https://jsonplaceholder.typicode.com/users';

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.heroesUrl)
      .pipe(
        delay(2000),
        map((users) => {

          this.users = uniqBy(this.usersFromStorage.concat(users), 'id')
            .sort((user, otherUser) => (user.id - otherUser.id) / Math.abs(user.id - otherUser.id));

          return this.users;
        }),
        tap(() => this.messageService.toastInfo('Fetched users')),
        catchError(this.handleError('getUsers', []))
      );
  }

  addUser(data: User): Observable<any> {
    if (!data.name || !data.username) {
      return throwError(new Error('Fill the data!'));
    }

    const user = new User(data);
    this.usersFromStorage = this.usersFromStorage.concat(user);

    return of(user).pipe(
      delay(1500)
    );
  }

  updateUser(id: number, data: object): Observable<User> {
    let updatedUser = this.users.find((user) => user.id === id);
    updatedUser = new User({
      ...updatedUser,
      ...data,
    });

    this.usersFromStorage = uniqBy([updatedUser].concat(this.usersFromStorage), 'id');

    return of(updatedUser).pipe(
      tap(() => {
        this.messageService.toastSuccess('User succesfully updated!');
      }),
      delay(1000),
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
