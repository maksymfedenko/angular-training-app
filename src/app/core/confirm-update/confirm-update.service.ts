import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Injectable()
export class ConfirmUpdateService {

  constructor(updates: SwUpdate) {
    console.warn('init update service');

    updates.available.subscribe(() => {
      if (confirm('These is a newer app version. Activate?')) {
        updates.activateUpdate().then(() => document.location.reload());
      }
    });
  }
}
