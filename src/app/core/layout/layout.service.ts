import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {
  isLayoutShown$: BehaviorSubject<boolean> = new BehaviorSubject(true);

  constructor() { }

  setLayoutShown(isLayoutShown: boolean) {
    this.isLayoutShown$.next(isLayoutShown);
  }
}
