import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './components/profile/profile.component';

const routes: Routes = [
  {
    path: '',
    component: ProfileComponent,
  }
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [ RouterModule.forChild(routes) ],
  declarations: []
})
export class ProfileRoutingModule { }
