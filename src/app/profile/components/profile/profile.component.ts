import { takeUntil } from 'rxjs/operators';
import { AuthService } from '../../../core/auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Profile } from '../../../shared/models/profile.model';
import { Unsubscriber } from '../../../shared/classes/unsubscriber.class';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends Unsubscriber implements OnInit {
  profile: Profile;

  constructor(
    private authService: AuthService,
    private location: Location
  ) {
    super ();
  }

  ngOnInit() {
    this.getProfile();
  }

  private getProfile(): void {
    this.authService.currentProfile$.pipe(
      takeUntil(this.componetDestroyed)
    )
      .subscribe(profile => this.profile = profile);
  }

  goBack(): void {
    this.location.back();
  }

  save(avatar): void {
    this.authService.updateProfile({
      ...this.profile,
      avatar,
    })
      .subscribe(() => this.goBack());
  }
}
