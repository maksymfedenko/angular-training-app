import { LayoutService } from '../../../core/layout/layout.service';
import { Router } from '@angular/router';
import { AuthService } from '../../../core/auth/auth.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnDestroy {
  authForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });
  submitting = false;
  submitError = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private layoutService: LayoutService,
  ) {
    this.layoutService.setLayoutShown(false);
  }

  ngOnDestroy() {
    this.layoutService.setLayoutShown(true);
  }

  submit(): void {
    this.submitting = true;
    this.submitError = '';

    this.authService.login(this.authForm.value)
      .subscribe(
        () => {
          this.submitting = false;
          this.router.navigate(['/users']);
        },
        (err) => {
          this.submitting = false;
          this.submitError = err.message;
        }
      );
  }
}
