import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from '../shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canDeactivate: [AuthGuard],
  }
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [ RouterModule.forChild(routes) ],
  declarations: []
})
export class AuthRoutingModule { }
