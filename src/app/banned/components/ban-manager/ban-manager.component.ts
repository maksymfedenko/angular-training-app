import { UsersService } from '../../../core/users/users.service';
import { Component, OnInit } from '@angular/core';
import { User } from '../../../shared/models/user.model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-ban-manager',
  templateUrl: './ban-manager.component.html',
  styleUrls: ['./ban-manager.component.scss']
})
export class BanManagerComponent implements OnInit {
  users: User[] = [];
  selectedUser: User;
  isLoading = false;

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.getUsers();
  }

  private getUsers(): void {
    this.isLoading = true;

    this.usersService.getUsers().pipe(
      finalize(() => {
        this.isLoading = false;
      })
    )
      .subscribe(users => {
        this.users = users;
      });
  }

  selectUser(id: string) {
    this.selectedUser = this.users.find((user) => user.id.toString() === id);
  }

  setBanned(banned: boolean) {
    this.usersService.updateUser(this.selectedUser.id, { banned }).subscribe();
  }
}
