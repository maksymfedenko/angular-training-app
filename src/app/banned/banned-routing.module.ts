import { AuthGuard } from './../shared/guards/auth.guard';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BanManagerComponent } from './components/ban-manager/ban-manager.component';
import { OnlyAdminGuard } from '../shared/guards/only-admin.guard';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: BanManagerComponent,
        canActivate: [OnlyAdminGuard],
      },
    ]
  },
];

@NgModule({
  exports: [
    RouterModule
  ],
  imports: [ RouterModule.forChild(routes) ],
  declarations: []
})
export class BannedRoutingModule { }
