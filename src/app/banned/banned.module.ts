import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BanManagerComponent } from './components/ban-manager/ban-manager.component';
import { BannedRoutingModule } from './banned-routing.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    BannedRoutingModule,
    SharedModule,
  ],
  declarations: [BanManagerComponent]
})
export class BannedModule { }
