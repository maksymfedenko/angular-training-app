import { ConfirmUpdateService } from './core/confirm-update/confirm-update.service';
import { LayoutService } from './core/layout/layout.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isSidebarOpen: boolean;
  isLayoutShown: boolean;

  constructor(private layoutService: LayoutService, private updateService: ConfirmUpdateService) { }

  ngOnInit() {
    this.layoutService.isLayoutShown$.subscribe(
      (isLayoutShown) => this.isLayoutShown = isLayoutShown
    );
  }

  toggleSidebar(isSidebarOpen: boolean) {
    this.isSidebarOpen = isSidebarOpen;
  }
}
